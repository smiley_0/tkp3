#!/usr/bin/env python3

import config
from modules.ui import Tkp3

if __name__ == '__main__':
    app = Tkp3()
    app.title(config.appname)

    app.mainloop()
