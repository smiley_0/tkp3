from random import randint

import config
import tkinter as tk
import tkinter.filedialog as fd
from modules.ccanvas import ClusterCanvas
from modules.generator import generate_file
from modules.tester import Tester

class ColLabel(tk.Label):
    def __init__(self, parent, **kwargs):
        super(ColLabel, self).__init__(
            parent,
            kwargs,
            fg=config.theme.text_fg,
            bg=config.theme.frame_bg,
            )



class Tkp3(tk.Tk):
    """main window"""
    def __init__(self):
        super(Tkp3, self).__init__()

        binp = fd.askopenfilename()

        self.init_ui()
        self.tester_instance = Tester(binp, self.can)

    def add_var(self, defval):
        val = tk.StringVar()
        val.set(defval)
        return val

    def add_entry(self, label, var, row):
        ColLabel(self.frm, text=label).grid(row=row, column=0, sticky="W")
        en = tk.Entry(self.frm, textvariable=var, highlightbackground=config.theme.frame_bg)
        en.grid(row=row, column=1)
        return en

    def init_ui(self):
        # Canvas
        self.can = ClusterCanvas(
            self,
            highlightthickness=0,
            width=config.win_height,
            height=config.win_height,
            bg=config.theme.canvas_bg,
        )
        self.can.grid(row=0, column=0)

        # Control panel
        self.frm = tk.Frame(
            self,
            highlightthickness=0,
            width=config.frm_width,
            height=config.win_height,
            bg=config.theme.frame_bg,
        )
        self.frm.grid_propagate(False)
        self.frm.grid(row=0, column=1)

        # Apptitle
        self.apt = tk.Label(
            self.frm,
            text=config.appname,
            fg=config.theme.text_fg,
            bg=config.theme.frame_bg,
            font=("sans-serif", 18),
        )
        self.apt.grid(row=0, column=0, columnspan=2, sticky="W")

        # Testing options
        self.vmin = self.add_var("5")
        self.inmin = self.add_entry("Min objects:", self.vmin, 1)

        self.vmax = self.add_var("42")
        self.inmax = self.add_entry("Max objects:", self.vmax, 2)

        self.vclust = self.add_var("4")
        self.inclust = self.add_entry("Clusters:", self.vclust, 3)

        # Generator parameters
        self.apt = tk.Label(
            self.frm,
            text="Generator parameters:",
            fg=config.theme.text_fg,
            bg=config.theme.frame_bg,
            font=("sans-serif", 16),
        )
        self.apt.grid(row=4, column=0, columnspan=2, sticky="W")

        self.vdiff = self.add_var("75")
        self.indiff = self.add_entry("Diffusion:", self.vdiff, 5)

        self.vspots = self.add_var("3")
        self.inspots = self.add_entry("Hotspots:", self.vspots, 6)

        self.vprob = self.add_var("47")
        self.inprob = self.add_entry("SpotProb:", self.vprob, 7)

        # Button
        self.btn = tk.Button(
            self.frm,
            text="Generate and test",
            command=self.act_gen_and_test,
            highlightbackground=config.theme.frame_bg,
        )
        self.btn.grid(row=20, column=0, columnspan=2, sticky="WENS")

        self.btn_ta = tk.Button(
            self.frm,
            text="Test again",
            command=self.act_test,
            highlightbackground=config.theme.frame_bg,
        )
        self.btn_ta.grid(row=30, column=0, columnspan=2, sticky="WENS")

    def act_gen_and_test(self):
        ivmin = int(self.vmin.get())
        ivmax = int(self.vmax.get())
        ivdiff = int(self.indiff.get())
        ivspots = int(self.inspots.get())
        ivprob = int(self.inprob.get())

        # generate input
        generate_file(ivmin, ivmax, ivdiff, ivspots, ivprob)

        self.act_test()

    def act_test(self):
        ivcls = int(self.vclust.get())

        # run test
        self.tester_instance.test(ivcls)
