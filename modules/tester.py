import subprocess
from re import split as resplit

import config


class Tester(object):
    def __init__(self, binary, canvas):
        super(Tester, self).__init__()
        self.binary = binary
        self.target_canvas = canvas

    def test(self, clusters):
        # prepare canvas
        self.target_canvas.delete("all")

        # run subprocess
        args = [self.binary, config.output_file, str(clusters)]
        print(args)
        p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)

        # parse output
        cur_cl = -1
        for x in p.stdout:
            print(x)
            for o in x.split():
                if o == b"cluster":
                    cur_cl += 1

                obj_str = resplit("[\[\],]", o.decode("utf-8"))
                try:
                    ob_id = int(obj_str[0])
                    x_c = int(obj_str[1])
                    y_c = int(obj_str[2])

                    self.target_canvas.draw_point(x_c, y_c, cur_cl)

                except Exception as e:
                    print(e)
                    print("not an object, skipping")


        p.wait()
