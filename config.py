from os import getcwd

# app name
appname = "Tkp3Alpha"

# window config
win_height = 1000
frm_width = 280

# generator config
output_file = '/'.join([getcwd(), "test.tmp"])

# canvas config
dot_radius = 10

# color theme
from themes import spacegray as theme
