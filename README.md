# Tkp3Alpha
> "Lebo hlupejsi nazov sa tazko vymysla"

## Requirements
- Python3.4 (cca) s Tkinter

## Basic usage
Nejakym sposobom spustit run.py. Terminal je overeny, teoreticky by mohol ist aj doubleclick vo windowse ale co ja viem. Potom vybehne velavravne okno na vyber suboru, treba vybrat binarku pre proj3. Je velmi vhodne skompilovat s `-DNDEBUG`, v opacnom pripade to bud nepojde (ak sa logy pisu na stdout) alebo pojde pomaly (ak sa logy pisu na stderr).

## Co su tie hluposti v sidebare?
- Min objects a max objects vravia za seba, hovoria generatoru rozsah pre pocet objektov z ktoreho si nahodne vygeneruje. Ak sa hodnoty rovnaju, obviously ziadna nahoda nebude. Ak je min > max, obviously exception (obviously neohandlovany).
- Clusters - pocet clusterov, nema vplyv na generator, iba sa posle ako argument pri spustani

## Co je ten zvysok?
Kedze obycajny randomak nehadze pekne hodnoty, trochu som tomu pomohol tym, ze s nejakou pravdepodobnostou sa novy object vygeneruje prave v tesnej blizkosti jedneho z predgenerovanych bodov (hotspotov).

- SpotProb urcuje pravdepodobnost v %, 0-100, floaty nezere
- Hotspots urcuje pocet hotspotov
- Diffusion je rozptyl suradnic objectu od suradnic hotspotu

## Co je ten zvysok v config.py?
Do toho vacsinou asi az tak sahat netreba, ale moze to byt zaujimave:

- `appname` - nazov ktory sa vypise do title
- `win_height` - vyska okna v px
- `frm_width` - sirka sidebaru v px, neni moc vhodne menit, akurat moze byt zaujimave vediet, ze `win_height` + `frm_width` = sirka celeho okna
- `output_file` - adresa docasneho suboru do ktoreho sa generuje vstup, default je v current working directory
- `dot_radius` - polomer vykresleneho bodu, i ked to zvadza k tomu ze je to v pixeloch, nope, je to scaleovane podla velkosti okna
- posledny riadok importuje temu zo zlozky themes, treba dodrzat format ale staci prepisat nazov (to medzi slovami `import` a `as`)
