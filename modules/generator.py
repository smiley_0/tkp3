from random import randint, random, choice
from math import sqrt

import config

CORD_MIN = 0
CORD_MAX = 1000

def generate_file(minobj, maxobj, diff, hotspot_c, spotprob):
    with open(config.output_file, "w") as f:
        obj = randint(minobj, maxobj)
        f.write("count={}\n".format(obj))

        hotspots = [(randint(CORD_MIN, CORD_MAX), randint(CORD_MIN, CORD_MAX)) for c in range(hotspot_c)]
        print("diff", diff, "hc", hotspot_c, "prob", spotprob, "spots", hotspots)

        for i in range(obj):
            #x = randint(CORD_MIN, CORD_MAX)
            #y = randint(CORD_MIN, CORD_MAX)

            # throw a dice
            d = (randint(0, 100) < spotprob)

            print("DICE", d)

            if d:
                xb, yb = choice(hotspots);
                print("hotspot", xb, yb)
                x = (xb + randint(-diff, diff)) % CORD_MAX
                y = (yb + randint(-diff, diff)) % CORD_MAX
            else:
                x = int((100*i*random())%CORD_MAX)
                y = int((100*i*random())%CORD_MAX)

            f.write("{} {} {}\n".format(i, x, y))
