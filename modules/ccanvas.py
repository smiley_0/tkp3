import config
import tkinter as tk

CANVAS_SIZE = 1000

class ClusterCanvas(tk.Canvas):
    """docstring for ClusterCanvas"""
    def __init__(self, master, *args, **kwargs):
        super(ClusterCanvas, self).__init__(master, kwargs)
        self.ratio = config.win_height / CANVAS_SIZE

    def draw_point(self, x, y, cluster):
        r = config.dot_radius

        # scale up/down
        x *= self.ratio
        y *= self.ratio
        r *= self.ratio

        self.create_oval(x-r, y-r, x+r, y+r,
            outline=config.theme.dot_border,
            fill=config.theme.colors[cluster]
        )
