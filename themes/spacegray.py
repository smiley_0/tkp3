canvas_bg = "#20242d"
frame_bg = "#1a1e25"

text_fg = "#b4b9c3"

dot_border = "#636671"

colors = [
    "#ff7f50",
    "#00bfff",
    "#ff69b4",
    "#90ee90",
    "#ba55d3",
    "#ffffe0",
    "#6b8e23",
    "#cd853f",
    "#bc8f8f",
    "#afeeee",
    "#dc143c",
    "#e0ffff",
    "#ffff00",
    "#9acd32",
    "#c0c0c0",
    "#ffd700",
    "#4682b4",
    "#ff0000",
    "#808000",
    "#ff00ff",
    "#00ff00",
    "#add8e6",
    "#f0e68c",
    "#000000",
]
